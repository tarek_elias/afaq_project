﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using AFAQ_Project.Classes;
namespace AFAQ_Project
{
    public partial class Login : System.Web.UI.Page
    {
        User _userClass;

        protected void Page_Load(object sender, EventArgs e)
        {
            login_label.Visible = false;
            _userClass = new User();
        }

    

        protected void login_btn_Click(object sender, EventArgs e)
        {
     
            _userClass.Username = myEmailText.Text.ToString();
            _userClass.Password = myPasswordText.Text.ToString();
            if (_userClass.loginIn())
            {
                    Session["admin"] = "yes";
                   Response.Redirect("Home.aspx");
         
            }
            else
            {
                login_label.Visible = true;
                
            }
         
        }
     
        protected void myPasswordText_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}