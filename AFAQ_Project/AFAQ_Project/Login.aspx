﻿<%@ Page Title="AFAQ Login" Language="C#" MasterPageFile="~/Navbar.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="AFAQ_Project.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


<link href="Content/custom_style.css" rel="stylesheet" type="text/css" />
   
<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <!-- Icon -->
    <div class="fadeIn first" id="myicon" style="margin:20px;">
      <img src="Assets/images/boss.svg" id="icon" alt="User Icon" />
    </div>

    <!-- Login Form -->
      <form runat="server">
  



    <asp:TextBox ID="myEmailText" runat="server" placeholder="email" TextMode="SingleLine" CssClass="textpass"></asp:TextBox>
     <asp:TextBox ID="myPasswordText" runat="server" CssClass=".textpass" TextMode="Password" placeholder="password" OnTextChanged="myPasswordText_TextChanged"
      ></asp:TextBox>
            <br />      
    <asp:Label ID="login_label" runat="server" Text="Invalid Email or Password" Style="margin:30px;" CssClass="alert alert-danger" Font-Italic="True" Font-Size="Small" ForeColor="#CC0000"></asp:Label>
            <br />
      <asp:Button ID="login_btn" OnClick="login_btn_Click" runat="server" Text="Login" UseSubmitBehavior="False" ViewStateMode="Enabled" CssClass="btn-primary btn" Style="margin:20px;"/>

          </form>
    
    <!-- Remind Passowrd -->
    <div id="formFooter">
      <a class="underlineHover" href="#">Forgot Password?</a>
    </div>

  </div>
      
</div>
 
</asp:Content>
