﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navbar.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="AFAQ_Project.About" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<h5>This is about page</h5>
<form runat="server" ID="p">
    <asp:Panel runat="server">
<asp:FileUpload ID="FileUpload1" runat="server" />
        <asp:RegularExpressionValidator ID="RegExValFileUploadFileType" runat="server"
                        ControlToValidate="FileUpload1"
                        ErrorMessage="Only .jpg,.png,.jpeg,.gif Files are allowed" Font-Bold="True"
                        Font-Size="Medium" CssClass="alert alert-warning"
                        ValidationExpression="(.*?)\.(jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF)$"></asp:RegularExpressionValidator>
    <asp:Button runat="server" Text="upload" OnClick="Unnamed1_Click" />  
</asp:Panel>
</form>
</asp:Content>
