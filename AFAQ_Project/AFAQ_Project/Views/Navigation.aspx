﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navbar.Master" AutoEventWireup="true" CodeBehind="Navigation.aspx.cs" Inherits="AFAQ_Project.Views.Navigation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" href="../Content/navigation_page_style.css" type="text/css" />
   <form runat="server">
    <div class="centerDiv">
        <table>
        <tr>
            <td>
                
                <asp:ImageButton ID="clientsButton1" runat="server" ImageUrl="~/Assets/images/people.png" CssClass="optionsImg" OnClick="clientsButton1_Click" />
                <br />
                <asp:Label Text="Clients Management" runat="server" on></asp:Label>
            </td>
            
            <td>
            
                <asp:ImageButton ID="realestatesButton2" ImageUrl="~/Assets/images/real-estate.png" runat="server" CssClass="optionsImg" OnClick="realestatesButton2_Click" />
                <br />
                <asp:Label Text="Real-Estates Management" runat="server"></asp:Label>    
            </td>
             </tr>
        <tr>
            <td>
                
                <asp:ImageButton ID="contractsButton3" ImageUrl="~/Assets/images/contract.png" runat="server" CssClass="optionsImg" OnClick="contractsButton3_Click" />
                <br />
                <asp:Label Text="Contracts Management" runat="server"></asp:Label>
            </td>

            <td>
                
                <asp:ImageButton ID="accountsButton4" ImageUrl="~/Assets/images/invoice.png" runat="server" CssClass="optionsImg" OnClick="accountsButton4_Click" />
                <br />
                <asp:Label Text="Accounts" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
        </div>
       </form>
</asp:Content>
