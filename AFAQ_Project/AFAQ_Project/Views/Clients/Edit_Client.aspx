﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navbar.Master" AutoEventWireup="true" CodeBehind="Edit_Client.aspx.cs" Inherits="AFAQ_Project.Views.Clients.Edit_Client" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <form runat="server">

    <asp:Table ID="Table1" runat="server">
    <asp:TableRow>
        <asp:TableCell><asp:Label ID="Label1" runat="server" Text="First Name"></asp:Label></asp:TableCell>
        <asp:TableCell><asp:TextBox ID="TextBox1" runat="server"></asp:TextBox></asp:TableCell>
    </asp:TableRow>

          <asp:TableRow>
        <asp:TableCell><asp:Label ID="Label2" runat="server" Text="Last Name"></asp:Label></asp:TableCell>
        <asp:TableCell><asp:TextBox ID="TextBox2" runat="server"></asp:TextBox></asp:TableCell>
    </asp:TableRow>

        
          <asp:TableRow>
        <asp:TableCell><asp:Label ID="Label3" runat="server" Text="Father's Name"></asp:Label></asp:TableCell>
        <asp:TableCell><asp:TextBox ID="TextBox3" runat="server"></asp:TextBox></asp:TableCell>
    </asp:TableRow>

        
          <asp:TableRow>
        <asp:TableCell><asp:Label ID="Label4" runat="server" Text="National ID Number"></asp:Label></asp:TableCell>
        <asp:TableCell><asp:TextBox ID="TextBox4" runat="server"></asp:TextBox></asp:TableCell>
    </asp:TableRow>

        
          <asp:TableRow>
        <asp:TableCell><asp:Label ID="Label5" runat="server" Text="Phone Number"></asp:Label></asp:TableCell>
        <asp:TableCell><asp:TextBox ID="TextBox5" runat="server"></asp:TextBox></asp:TableCell>
    </asp:TableRow>


        
          <asp:TableRow>
        <asp:TableCell><asp:Label ID="Label6" runat="server" Text="Birth Date"></asp:Label></asp:TableCell>
        <asp:TableCell>
            <asp:Calendar ID="birthdate_Calender" runat="server"></asp:Calendar>
        </asp:TableCell>
    </asp:TableRow>

        
          <asp:TableRow>
        <asp:TableCell><asp:Label ID="Label7" runat="server" Text="Gender"></asp:Label></asp:TableCell>
        <asp:TableCell><asp:DropDownList ID="gender_DropDownList1" runat="server"></asp:DropDownList></asp:TableCell>
    </asp:TableRow>

        
          <asp:TableRow>
        <asp:TableCell><asp:Label ID="Label8" runat="server" Text="Client Type"></asp:Label></asp:TableCell>
        <asp:TableCell><asp:DropDownList ID="clientType_DropDownList1" runat="server"></asp:DropDownList></asp:TableCell>
    </asp:TableRow>

          <asp:TableRow>
        <asp:TableCell><asp:Label ID="Label9" runat="server" Text="Country"></asp:Label></asp:TableCell>
        <asp:TableCell><asp:TextBox ID="country_TextBox6" runat="server"></asp:TextBox></asp:TableCell>
    </asp:TableRow>
        <asp:TableRow>
        <asp:TableCell><asp:Label ID="Label10" runat="server" Text="City"></asp:Label></asp:TableCell>
        <asp:TableCell><asp:TextBox ID="city_TextBox7" runat="server"></asp:TextBox></asp:TableCell>
    </asp:TableRow>
        
        <asp:TableRow>
        <asp:TableCell><asp:Label ID="Label11" runat="server" Text="District"></asp:Label></asp:TableCell>
        <asp:TableCell><asp:TextBox ID="district_TextBox8" runat="server"></asp:TextBox></asp:TableCell>
    </asp:TableRow>

        
        <asp:TableRow>
        <asp:TableCell><asp:Label ID="Label12" runat="server" Text="Street"></asp:Label></asp:TableCell>
        <asp:TableCell><asp:TextBox ID="street_TextBox9" runat="server"></asp:TextBox></asp:TableCell>
    </asp:TableRow>

        
        <asp:TableRow>
        <asp:TableCell><asp:Label ID="Label13" runat="server" Text="Street Number"></asp:Label></asp:TableCell>
        <asp:TableCell><asp:TextBox ID="streetNum_TextBox10" runat="server"></asp:TextBox></asp:TableCell>
    </asp:TableRow>
      


           <asp:TableRow>
               <asp:TableCell>
               <asp:Button ID="submitBtn" runat="server" Text="Submit Changes" OnClick="submitBtn_Click" style="width:100%;" />
                </asp:TableCell>
                   </asp:TableRow>

              <asp:TableRow>
               <asp:TableCell>
               <asp:Button ID="addBtn" runat="server" Text="Add Client" OnClick="addBtn_Click" style="width:100%;" />
                </asp:TableCell>
                   </asp:TableRow>

    </asp:Table>

        </form>





</asp:Content>
