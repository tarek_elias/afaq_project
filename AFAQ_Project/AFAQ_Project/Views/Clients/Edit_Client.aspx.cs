﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
namespace AFAQ_Project.Views.Clients
{
    public partial class Edit_Client : System.Web.UI.Page
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["AFAQConnectionString_login1"].ConnectionString);

        public string gottedId = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            gender_DropDownList1.Items.Add("Male");
            gender_DropDownList1.Items.Add("Female");
            clientType_DropDownList1.Items.Add("Buyer");
            clientType_DropDownList1.Items.Add("Seller");

            if (!IsPostBack)
            {

                string _qs =  Request.QueryString["id"].ToString();
                if (_qs == "CREATION")
                {
                    submitBtn.Visible = false;

                }
                else
                {
                    addBtn.Visible = false;
                    fillInfo(_qs);
                    gottedId = _qs;
                }
            }
            
        }

        public void fillInfo(string retId)
        {
            string[] returend = retId.Split('-');
            string id = returend[0];
            string addId = returend[1];

            conn.Open();
            string checkUser = "select * from customers where cus_id="+id;
            SqlCommand myCommand = new SqlCommand(checkUser, conn);
            SqlDataReader sqlDataReader = myCommand.ExecuteReader();
            if (sqlDataReader.Read())
            {
                TextBox1.Text = sqlDataReader["first_name"].ToString();
                TextBox2.Text = sqlDataReader["last_name"].ToString();
                TextBox3.Text = sqlDataReader["father_name"].ToString();
                TextBox4.Text = sqlDataReader["national_id"].ToString();
                TextBox5.Text = sqlDataReader["phone_num"].ToString();
                DateTime birthDate;
                DateTime.TryParse(sqlDataReader["birth_date"].ToString(),out birthDate);
                birthdate_Calender.SelectedDate = birthDate;
                if (sqlDataReader["gender"].ToString() == "male" || sqlDataReader["gender"].ToString() == "Male")
                {
                    gender_DropDownList1.SelectedIndex = 0;
                }
                else
                {
                    gender_DropDownList1.SelectedIndex = 1;
                }

                if (sqlDataReader["cus_type"].ToString() == "buyer" || sqlDataReader["cus_type"].ToString() == "Buyer")
                {
                    clientType_DropDownList1.SelectedIndex = 0;
                }
                else
                {
                    clientType_DropDownList1.SelectedIndex = 1;
                }
            }
            sqlDataReader.Close();
            string checkAddress = "select * from [addresses] where id=" + addId;
            SqlCommand myCommand2 = new SqlCommand(checkAddress, conn);
            SqlDataReader sqlDataReader2 = myCommand2.ExecuteReader();
            if (sqlDataReader2.Read())
            {
                country_TextBox6.Text = sqlDataReader2["country"].ToString();
                city_TextBox7.Text = sqlDataReader2["city"].ToString();
                district_TextBox8.Text = sqlDataReader2["district"].ToString();
                street_TextBox9.Text = sqlDataReader2["street"].ToString();
                streetNum_TextBox10.Text = sqlDataReader2["number"].ToString();
            }
            conn.Close();
        }

        public void updateValues()
        {

            string[] returend = Request.QueryString["id"].ToString().Split('-');
            string id = returend[0];
            string addId = returend[1];
            
            conn.Open();
            string sqlQ = "UPDATE [customers] SET [first_name] = @FirstName," +
                "  [last_name] = @LastName," +
                " [father_name] = @FatherName," +
                " [national_id] = @NationalId," +
                " [phone_num] = @PhoneNumber," +
                " [birth_date] = @BirthDate," +
                " [gender] = @Gender," +
                " [cus_type] = @CustomerType" +
                "  WHERE [cus_id]=" + id;

            try
            {
                using (SqlCommand myCommand = new SqlCommand(sqlQ, conn))
                {
                    // myCommand.Parameters.AddWithValue("@CustomerID", SqlDbType.Int).Value = Request.QueryString["id"];
                    myCommand.Parameters.AddWithValue("@FirstName", SqlDbType.NVarChar).Value = TextBox1.Text.ToString();
                    myCommand.Parameters.AddWithValue("@LastName", SqlDbType.NVarChar).Value = TextBox2.Text.ToString();
                    myCommand.Parameters.AddWithValue("@FatherName", SqlDbType.NVarChar).Value = TextBox3.Text.ToString();
                    myCommand.Parameters.AddWithValue("@NationalId", SqlDbType.NVarChar).Value = TextBox4.Text.ToString();
                    myCommand.Parameters.AddWithValue("@PhoneNumber", SqlDbType.NVarChar).Value = TextBox5.Text.ToString();
                    myCommand.Parameters.AddWithValue("@BirthDate", SqlDbType.Date).Value = birthdate_Calender.SelectedDate.ToString();
                    myCommand.Parameters.AddWithValue("@Gender", SqlDbType.NVarChar).Value = gender_DropDownList1.SelectedItem.Text.ToString();
                    myCommand.Parameters.AddWithValue("@CustomerType", SqlDbType.NVarChar).Value = clientType_DropDownList1.SelectedItem.Text.ToString();

                    myCommand.ExecuteNonQuery();
                    myCommand.Dispose();
                }
            }
            catch (Exception e)
            {
                Response.Write(e.Message.ToString());
            }
            string sqlQ2 = "UPDATE [addresses] set [country] = @Country," +
                    "[city] = @City," +
                    "[district] = @District," +
                    "[street] = @Street," +
                    "[number] = @StreetNum" +
                    " WHERE [id] = " + addId;
            try
            {
                using (SqlCommand myCommand2 = new SqlCommand(sqlQ2, conn))
                {
                    myCommand2.Parameters.AddWithValue("@Country", SqlDbType.NVarChar).Value = country_TextBox6.Text.ToString();
                    myCommand2.Parameters.AddWithValue("@City", SqlDbType.NVarChar).Value = city_TextBox7.Text.ToString();
                    myCommand2.Parameters.AddWithValue("@District", SqlDbType.NVarChar).Value = district_TextBox8.Text.ToString();
                    myCommand2.Parameters.AddWithValue("@Street", SqlDbType.NVarChar).Value = street_TextBox9.Text.ToString();
                    myCommand2.Parameters.AddWithValue("@StreetNum", SqlDbType.NVarChar).Value = streetNum_TextBox10.Text.ToString();
                    myCommand2.ExecuteNonQuery();
                }
            }
            catch (Exception w)
            {
                Response.Write(w.Message.ToString());
            }



                conn.Close();
                Response.Redirect("Show.aspx");
            
            
        }

        public void addClient(string ID)
        {
            //conn.Open();
            string sqlQ = "INSERT INTO [customers] VALUES (@FirstName," +
                "@LastName," +
                "@FatherName," +
                "@Address_id,"+
                "@NationalId," +
                "@PhoneNumber," +
                "@BirthDate," +
                "@Gender," +
                "@CustomerType," +
                "@CreatedAt"+
                ")";

            try
            {
                using (SqlCommand myCommand = new SqlCommand(sqlQ, conn))
                {
                    myCommand.Parameters.AddWithValue("@FirstName", SqlDbType.NVarChar).Value = TextBox1.Text.ToString();
                    myCommand.Parameters.AddWithValue("@LastName", SqlDbType.NVarChar).Value = TextBox2.Text.ToString();
                    myCommand.Parameters.AddWithValue("@FatherName", SqlDbType.NVarChar).Value = TextBox3.Text.ToString();
                    myCommand.Parameters.AddWithValue("@Address_id", SqlDbType.Int).Value = Convert.ToInt32(ID);
                    myCommand.Parameters.AddWithValue("@NationalId", SqlDbType.NVarChar).Value = TextBox4.Text.ToString();
                    myCommand.Parameters.AddWithValue("@PhoneNumber", SqlDbType.NVarChar).Value = TextBox5.Text.ToString();
                    myCommand.Parameters.AddWithValue("@BirthDate", SqlDbType.Date).Value = birthdate_Calender.SelectedDate.ToString();
                    myCommand.Parameters.AddWithValue("@Gender", SqlDbType.NVarChar).Value = gender_DropDownList1.SelectedItem.Text.ToString();
                    myCommand.Parameters.AddWithValue("@CustomerType", SqlDbType.NVarChar).Value = clientType_DropDownList1.SelectedItem.Text.ToString();
                    myCommand.Parameters.AddWithValue("@CreatedAt", SqlDbType.DateTime).Value = DateTime.Now.ToString();
                    myCommand.ExecuteNonQuery();
                }
                conn.Close();
               Response.Redirect("Show.aspx");
            }
            catch (Exception e)
            {
                Response.Write(" " + e.Message.ToString());
            }
        }
        public void addAddress()
        {
            conn.Open();
            string sqlQ = "INSERT INTO [addresses] VALUES (@Country," +
                "@City," +
                "@District," +
                "@Street," +
                "@StreetNum" + 
                "); SELECT SCOPE_IDENTITY()";

            try
            {
                using (SqlCommand myCommand = new SqlCommand(sqlQ, conn))
                {
                    myCommand.Parameters.AddWithValue("@Country", SqlDbType.NVarChar).Value = country_TextBox6.Text.ToString();
                    myCommand.Parameters.AddWithValue("@City", SqlDbType.NVarChar).Value = city_TextBox7.Text.ToString();
                    myCommand.Parameters.AddWithValue("@District", SqlDbType.NVarChar).Value = district_TextBox8.Text.ToString();
                    myCommand.Parameters.AddWithValue("@Street", SqlDbType.NVarChar).Value = street_TextBox9.Text.ToString();
                    myCommand.Parameters.AddWithValue("@StreetNum", SqlDbType.NVarChar).Value = streetNum_TextBox10.Text.ToString();
                    var res = myCommand.ExecuteScalar();
                    addClient(res.ToString());
                }
    
            }
            catch (Exception e)
            {
                Response.Write(" " + e.Message.ToString());
            }

        }
        protected void submitBtn_Click(object sender, EventArgs e)
        {
            updateValues();
           
        }

        protected void addBtn_Click(object sender, EventArgs e)
        {
            addAddress();
        }
    }
}