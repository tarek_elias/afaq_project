﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace AFAQ_Project
{
    public partial class Clients : System.Web.UI.Page
    {

        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["AFAQConnectionString_login1"].ConnectionString);
        

        protected void Page_Load(object sender, EventArgs e)
        {
           
            fetchClients();
            deletionPanel.Visible = false;
            showaddress_btn.Visible = false;
        }

        public void fetchClients()
        {          
            conn.Open();
            string checkUser = "select * from customers";
            SqlCommand myCommand = new SqlCommand(checkUser, conn);
            SqlDataReader sqlDataReader = myCommand.ExecuteReader();
            GridView1.DataSource = sqlDataReader;
            GridView1.DataBind();
            conn.Close();
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView1.SelectedRow.ForeColor = System.Drawing.Color.Green;
            ViewState["address_id"] = GridView1.SelectedRow.Cells[5].Text.ToString();
            showaddress_btn.Visible = true;

        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            
            string id = GridView1.Rows[e.NewEditIndex].Cells[1].Text;
            string addId = GridView1.Rows[e.NewEditIndex].Cells[5].Text;
            Response.Redirect("Edit_Client.aspx?id="+id+ "-" +addId);

        }

        public void deleteCustomer(string cusId)
        {
            
                Response.Write("entering...");
                string sqlString = "DELETE FROM customers WHERE cus_id = @ID";
                using (var cmd1 = new SqlCommand(sqlString, conn))
                {
                    cmd1.Parameters.Add("@ID", SqlDbType.Int).Value = Convert.ToInt32(cusId);
                    conn.Open();
                    cmd1.ExecuteNonQuery();
                }
                conn.Close();
            Response.Redirect("Show.aspx");

        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            deletionPanel.Visible = true;
            GridView1.Enabled = false;
            ViewState["selected_id"] = string.Empty;
            ViewState["selected_id"] = GridView1.Rows[e.RowIndex].Cells[1].Text.ToString();
        }

        protected void goToAddClient_Click(object sender, EventArgs e)
        {
            Response.Redirect("Edit_Client.aspx?id=CREATION");
        }
   
        protected void deleteConfim_btn_Click(object sender, EventArgs e)
        {
            deleteCustomer(ViewState["selected_id"].ToString());
        }

        protected void deleteCancel_btn_Click(object sender, EventArgs e)
        {
            deletionPanel.Visible = false;
            GridView1.Enabled = true;
            ViewState["selected_id"] = string.Empty;
        }

        protected void showaddress_btn_Click(object sender, EventArgs e)
        {
            getAddressInfo(ViewState["address_id"].ToString());
           
        }

        public void getAddressInfo(string id)
        {
            conn.Open();
            string checkUser = "select * from [addresses] where id="+id;
            SqlCommand myCommand = new SqlCommand(checkUser, conn);
            SqlDataReader sqlDataReader = myCommand.ExecuteReader();
            address_GridView2.DataSource = sqlDataReader;
            address_GridView2.DataBind();
            conn.Close();
        }


        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            
        }
    }
}
