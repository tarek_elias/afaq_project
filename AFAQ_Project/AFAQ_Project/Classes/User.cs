﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace AFAQ_Project.Classes
{
    public class User
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public User()
        { 
        
        }

        public int checkUser(string em, string pass)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["AFAQConnectionString_login1"].ConnectionString);
            conn.Open();
            string checkUser = "select count(*) from users where email= '" + em + "' and password= '" + pass + "'";
            SqlCommand myCommand = new SqlCommand(checkUser, conn);
            int temp = Convert.ToInt32(myCommand.ExecuteScalar().ToString());
            conn.Close();
            return temp;
        }


        public static byte[] GetHash(string inputString)
        {
            using (HashAlgorithm algorithm = SHA256.Create())
                return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

        public bool loginIn()
        {
            try
            {
                //String enteredEmail = myEmailText.Text.ToString();
                string enteredEmail = Username;
                if (checkUser(enteredEmail, GetHashString(Password)) > 0)
                {
                    return true;
                }
                else
                {
                    return false;

                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

    }
}